# Learn Cooperation  

- [x] How to fix issues. see [issue#1](https://gitlab.com/_KuoNuoDioDa_/hello-issue/-/issues/1).  
- [x] A more formal commit message. see [issue#2](https://gitlab.com/_KuoNuoDioDa_/hello-issue/-/issues/2).  
- [x] Merge request first, push second. see [issue#3](https://gitlab.com/_KuoNuoDioDa_/hello-issue/-/issues/3).  
- [x] Fixed more than one issue in one MR, see issue #8 #9  
- [x] Merge 3 commits
- [x] Merge 2 lines  
- [x] Merge 2 lines  

## 怎么优雅的解决issue  

先在本地仓库创建一个分支，然后在这个分支上改动（当然也可以直接在gitlab web端使用下面的方式新建合并请求同时创建分支，最后都需要 `git pull` 同步远程和本地分支）  

![merge-request.png](./merge-request.png)  

在相应的issue界面创建merge request时会创建一个分支，这里填上你平时解决issue的分支就行了，然后本地先得用`git pull`同步一下分支，然后在相应的分支push上来就会自动关联到这个mr，一般创建的mr都是Draft（草稿状态），这个状态可以代表你正在解决这个问题但是还未完成，同时管理员也没办法merge你的commit，完成之后需要自己手动Mark as Ready，这时候才能被合并  

解决两个疑问  
Q:为什么在相应issue界面创建mr？因为我先上传代码后创建mr有个直接从我提交的分支创建mr的功能。  
A:很好，那也是一种方式，我们也可以通过commit信息里写的issue号直接锁定到issue，这样不是不好，这样更方便，但是细节方面比先创建mr来讲少了一些，首先就是标题，如果先push后创建mr，标题会直接引用你的commit信息，这样我们没法在浏览问题时直观看到你解决什么问题，其次是issue的标签，先push然后通过push的分支创建mr没办法自动添加标签，当然你也可以手动添加，不过自动的岂不是更爽？重点是不易出错。另外像自动生成的Draft、Solve标题标识正在解决的状态就不说了，下面也有提到。  
  
Q:为什么创建mr时要填上和本地分支一样的名字？  
A:要知道你所填的分支名会在远程仓库被创建出来。关于问题原因有二，一个是能在你提交代码后自动关联到这个mr，另一个是如果仓库有相同的分支名字则那个分支不能被创建，我习惯用一个固定的分支名解决问题，这些分支在合并之后会被自动删除（这个功能是默认生效的），所以每次都得填一遍。另外，创建mr时会在仓库创建的分支名，你也可以使用默认生成的分支，这样每解决一个issue就多一个分支（虽然都会被删除）  

## 一条优雅的commit message  

git -s -m"项目:[#issue号码] 描述"  
如同解决 issue#2 的 [commit](https://gitlab.com/_KuoNuoDioDa_/hello-issue/-/merge_requests/2/diffs?commit_id=551250553ce1cb942509a67baa5d958341f2dd59)，同时需要加上自己的邮箱("-s"参数自动解决)  
在此之前最好确定一下自己的邮箱  

```bash
git config --get user.email
git commit -s -m"hello-issue:[#1] This is commit message. -s to add email automaticly"  
```  

要注意一件事，分支上的所有commit一般都会被合并到主分支上（可以只合并最后一个commit），所以最好先在本地把改动都确认好，以免上传不必要的修改（改动很小，重复标题，影响观感）  

对于一个问题多次提交的状况，提交了多个commit，我不知道merge之后会有多个commit还是会自动合并成一个  

如果有多个，我们可以用`git rebase`命令解决这个问题，以后记录在问题修复里  

## 修复问题  

如果你不小心commit到了错误的分支，你可以使用下面的命令回到上一个版本但是保留所有的修改，同时撤销掉此次的commit  

```bash
git reset --soft HEAD^
```  
